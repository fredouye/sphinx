# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html


# -- Project information -----------------------------------------------------

project = "CDC Sophia Antipolis"
copyright = '2022, Sopra Steria'
author = 'Frédéric Mangeant'

# The full version, including alpha/beta/rc tags
release = 'https://doc.fr.ghc.local'
#version = '1.0'

language = 'en'

html_logo = 'source/logo.png'
html_last_updated_fmt = '%d/%m/%Y %H:%M %Z'

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.

#import sphinx_rtd_theme

extensions = [
    'sphinx_rtd_theme',
    'recommonmark',
    'sphinx_last_updated_by_git',
    'sphinx_copybutton',
    'rinoh.frontend.sphinx',
#    'myst_parser',
#    'sphinx_immaterial'
#    'sphinx_design'
#    'sphinx_tabs.tabs',
#    'sphinx_toolbox.collapse',
#    'rst2pdf.pdfbuilder'
#    'sphinx_search.extension'
#    'sphinxcontrib.lunrsearch'
]

myst_enable_extensions = ["colon_fence"]

#pdf_documents = [('index', u'rst2pdf', u'Sample rst2pdf doc', u'Your Name'),]

#latex_documents = [
#    ('index', 'toto.pdf', 'toto',
#     'titi', 'manual'),
#]

# output.pdf
# https://www.mos6581.org/rinohtype/0.5.3/sphinx.html
rinoh_documents = [dict(doc='index',target='output',title='SSG documentation',subtitle='SSG',author='CDC Sophia')]

source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown'
}

source_parsers = {
    '.md' : 'recommonmark.parser.CommonMarkParser',
}

#html_theme = 'sphinx_immaterial'
html_theme = "sphinx_rtd_theme"

html_theme_options = {
    "navigation_depth": 4,
#    "style_nav_header_background": "#5bbdbf",
    "style_external_links": True,
    'collapse_navigation': False,
    'sticky_navigation': False,
#    'display_version': True
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
#html_theme = 'alabaster'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
