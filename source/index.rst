.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. Welcome to GitLab Pages with Fredouye's documentation!
.. ====================================================

.. raw:: html

    <div align="right";>

:download:`PDF<pdf/doc.pdf>`

.. raw:: html

    <div align="left";>


.. toctree::
   :maxdepth: 2
   :caption: Contents:

Technologies
============

.. toctree::
   :maxdepth: 1

   Linux <technologies/linux/index>
   Windows <technologies/windows/index>
   Network <technologies/network/index>
   Storage <technologies/storage/index>
   Databases <technologies/databases/index>
   Cloud <technologies/cloud/index>
   Virtualization <technologies/virtualization/index>
